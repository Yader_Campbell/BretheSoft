package com.example.brethe_soft;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }
    //Metodo que me envia al activity Biblografia
    public void biblografia(View view){

        Intent b = new Intent(this,biblografia.class);
        startActivity(b);

    }

    //Metodo que me envia al activity Acerca
    public void acerca(View view){

        Intent a = new Intent(this,acerca.class);
        startActivity(a);

    }

}
