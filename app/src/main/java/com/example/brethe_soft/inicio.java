package com.example.brethe_soft;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class inicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }

    //Metodo para el button de crear un nuevo usurio
    public void Crear(View view){

        Intent i = new Intent(this, crear_usurio.class);
        startActivity(i);

        Intent p = new Intent(this,ScreenSplash.class);
        startActivity(p);
    }

    //Metodo para ingresar un viejo usurio
    public void Ingresar(View view){
        Intent i = new Intent(this, Login_existe.class);
        startActivity(i);
    }
}
