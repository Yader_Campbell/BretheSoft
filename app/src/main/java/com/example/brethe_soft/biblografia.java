package com.example.brethe_soft;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class biblografia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biblografia);
    }

    //Metodo para el button de silivantes
    public void silivante(View view){
        MediaPlayer silvante = MediaPlayer.create(this, R.raw.sibilancia_adulto);
        silvante.start();
    }

    public void crepito(View view){
        MediaPlayer crepito = MediaPlayer.create(this, R.raw.crepito_latente);
        crepito.start();
    }
}
